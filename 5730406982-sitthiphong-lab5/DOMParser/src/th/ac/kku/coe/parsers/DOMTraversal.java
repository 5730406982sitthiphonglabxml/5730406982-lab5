package th.ac.kku.coe.parsers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DOMTraversal {

    public static void main(String[] args) {
        
        String filename = "nation.xml";
        DocumentBuilderFactory docfactory = DocumentBuilderFactory.newInstance();
        try {
            
            DocumentBuilder docbuild = docfactory.newDocumentBuilder();
            Document docNode = docbuild.parse(filename);
            followNode(docNode);
            
        } catch (Exception ex) {

        }
    }

    public static void followNode(Node node) throws IOException {
        
        writeNode(node);
        if ( node.hasChildNodes() ) {
            String nodename = node.getNodeName();
            int num = node.getChildNodes().getLength();
            System.out.println("node " + nodename + " has " + num + " chlidren");
            Node fst = node.getFirstChild();
            
            followNode(fst);
        }
        
        Node xnode = node.getNextSibling();
        if (xnode != null) {
            followNode(xnode);
        }
        
    }

    public static void writeNode(Node node) {
        System.out.println("Node:type = " + node.getNodeType() + " name = " + node.getNodeName() + " value = " + node.getNodeValue());
    }

}
