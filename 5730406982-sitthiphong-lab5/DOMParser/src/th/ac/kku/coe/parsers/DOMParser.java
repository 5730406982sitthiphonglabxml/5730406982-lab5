package th.ac.kku.coe.parsers;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DOMParser {

    public static void main(String[] args) {
        
        String filename = "nation.xml";
        DocumentBuilderFactory docfactory = DocumentBuilderFactory.newInstance();
        
        try {
            
            DocumentBuilder docBuild = docfactory.newDocumentBuilder();
            Document docNode = docBuild.parse(filename);
            Element rootE = docNode.getDocumentElement();
            
            System.out.println("The root element name is " + rootE.getNodeName());
            
        } catch (Exception ex) {

        }
    }
}
