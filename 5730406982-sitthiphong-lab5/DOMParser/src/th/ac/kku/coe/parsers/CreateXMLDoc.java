package th.ac.kku.coe.parsers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


public class CreateXMLDoc {

    public static void main(String[] args) {
        try {
            String xmlProfile = "myProfile.xml";
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();
            Element rootnode = document.createElement("profile");
            Element name = document.createElement("name");
            Text myname = document.createTextNode("Sitthiphong Kanhasura");
            document.appendChild(rootnode);
            rootnode.appendChild(name);
            name.appendChild(myname);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource doms = new DOMSource(document);
            
            StreamResult stResult = new StreamResult(new File("C:\\Users\\ASUS\\Documents\\NetBeansProjects\\5730406982-sitthiphong-lab5\\src\\th\\ac\\kku\\coe\\parsers\\" + xmlProfile));
            transformer.transform(doms, stResult);

            System.out.println(" File Saved ");

        } catch (Exception ex) {
            
            System.err.println(" File Saved Error ");
            
        }
    }

}
